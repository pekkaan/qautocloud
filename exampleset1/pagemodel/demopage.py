# -*- coding: utf-8 -*-
# Example for using WebDriver object: driver = self.get_current_driver() e.g driver.current_url
from selenium.webdriver.common.by import By
from QAutoLibrary.QAutoSelenium import *
from QAutoLibrary.extension.parsers.parameter_parser import get_parameter
from time import sleep

class Demopage(CommonUtils):
    # Pagemodel timestamp: 20180407162304
    # Pagemodel url: https://demo.qautomate.fi/
    # Pagemodel area: Full screen
    # Pagemodel screen resolution: (1920, 1080)
    # Use project settings: True
    # Used filters: default
    # Xpath type: xpath-position
    # Create automated methods: True
    # Depth of css path: 7
    # Minimize css selector: False
    # Use css pattern: False
    # Allow non unique css pattern: False
    # Pagemodel template: False
    # Use testability: False
    # Use contains text in xpath: False
    # Exclude dynamic table filter: True
    # Row count: 25
    # Element count: 140
    # Big element filter width: 55
    # Big element filter height: 40
    # Not filtered elements: button, select, strong, input, frame
    # Canvas modeling: False
    # Pagemodel type: normal
    # Links found: 1
    # Page model constants:
    FLOAT_LEFT_ALT_LOGO = (By.CSS_SELECTOR, u'a.floatLeft>img[alt="logo"]') # x: 301 y: 40 width: 91 height: 90, tag: img, type: , name: None, form_id: , checkbox: , table_id: , href: http://www.qautomate.fi/
    ID_LOGO_PIC = (By.ID, u'logo_pic') # x: 674 y: 40 width: 470 height: 80, tag: img, type: , name: None, form_id: , checkbox: , table_id: , href: None
    LOGIN_CUSTOMER = (By.CSS_SELECTOR, u'p.login_header') # x: 1422 y: 50 width: 180 height: 11, tag: p, type: , name: None, form_id: , checkbox: , table_id: , href:
    ALT_LOGIN = (By.CSS_SELECTOR, u'img[alt="Login"]') # x: 1530 y: 71 width: 75 height: 31, tag: img, type: , name: None, form_id: , checkbox: , table_id: , href: https://demo.qautomate.fi/login.html
    HOME = (By.LINK_TEXT, u'Home') # x: 932 y: 155 width: 96 height: 40, tag: a, type: , name: None, form_id: , checkbox: , table_id: , href: http://www.qautomate.fi/
    ALT_LOGO = (By.CSS_SELECTOR, u'h1>img[alt="logo"]') # x: 812 y: 245 width: 296 height: 50, tag: img, type: , name: None, form_id: , checkbox: , table_id: , href: None
    LOGIN = (By.CSS_SELECTOR, u'h2') # x: 785 y: 400 width: 350 height: 34, tag: h2, type: , name: None, form_id: j_spring_security_check, checkbox: , table_id: , href:
    ID_USERNAME_INPUTLEFT = (By.ID, u'username_inputleft') # x: 810 y: 498 width: 12 height: 50, tag: div, type: , name: None, form_id: j_spring_security_check, checkbox: , table_id: , href:
    USERNAME_INPUTMIDDLE_URL_J_TEXT = (By.CSS_SELECTOR, u'#username_inputmiddle>#url') # x: 822 y: 498 width: 296 height: 47, tag: input, type: text, name: j_username, form_id: j_spring_security_check, checkbox: , table_id: , href:
    ID_USERNAME_INPUTRIGHT = (By.ID, u'username_inputright') # x: 1098 y: 498 width: 12 height: 50, tag: div, type: , name: None, form_id: j_spring_security_check, checkbox: , table_id: , href:
    ID_USER_PIC = (By.ID, u'user_pic') # x: 822 y: 517 width: 26 height: 10, tag: img, type: , name: None, form_id: j_spring_security_check, checkbox: , table_id: , href: None
    ID_PASSWORD_INPUTLEFT = (By.ID, u'password_inputleft') # x: 810 y: 558 width: 12 height: 50, tag: div, type: , name: None, form_id: j_spring_security_check, checkbox: , table_id: , href:
    PASSWORD_INPUTMIDDLE_URL_J = (By.CSS_SELECTOR, u'#password_inputmiddle>#url') # x: 822 y: 558 width: 296 height: 47, tag: input, type: password, name: j_password, form_id: j_spring_security_check, checkbox: , table_id: , href:
    ID_PASSWORD_PIC = (By.ID, u'password_pic') # x: 825 y: 573 width: 9 height: 17, tag: img, type: , name: None, form_id: j_spring_security_check, checkbox: , table_id: , href: None
    ID_SUBMIT = (By.ID, u'submit') # x: 816 y: 618 width: 299 height: 41, tag: input, type: image, name: None, form_id: j_spring_security_check, checkbox: , table_id: , href:

    def input_details(self, parameters=None):
        self.input_text(self.USERNAME_INPUTMIDDLE_URL_J_TEXT, parameters['j_username'])
        self.input_text(self.PASSWORD_INPUTMIDDLE_URL_J, parameters['j_password'])

    def submit_jspringsecuritycheck(self):
        self.click_element(self.ID_SUBMIT)
