# -*- coding: utf-8 -*-
# Example for using WebDriver object: driver = self.get_current_driver() e.g driver.current_url
from selenium.webdriver.common.by import By
from QAutoLibrary.QAutoSelenium import *
from QAutoLibrary.extension.parsers.parameter_parser import get_parameter
from time import sleep

class Open_application(CommonUtils):
    # Pagemodel timestamp: 20180407162228
    # Pagemodel url: file:///C:/Python27/Lib/site-packages/qautorobot/webframework/resources/startpage/start.html
    # Pagemodel area: Full screen
    # Pagemodel screen resolution: (1920, 1080)
    # Use project settings: True
    # Used filters: default
    # Xpath type: xpath-position
    # Create automated methods: True
    # Depth of css path: 7
    # Minimize css selector: False
    # Use css pattern: False
    # Allow non unique css pattern: False
    # Pagemodel template: False
    # Use testability: False
    # Use contains text in xpath: False
    # Exclude dynamic table filter: True
    # Row count: 25
    # Element count: 140
    # Big element filter width: 55
    # Big element filter height: 40
    # Not filtered elements: button, select, strong, input, frame
    # Canvas modeling: False
    # Pagemodel type: root
    # Links found: 0
    # Page model constants:
    
    def open_application_url(self, url):
        self.open_url(url)


